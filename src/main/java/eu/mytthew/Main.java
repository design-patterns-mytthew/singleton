package eu.mytthew;

public class Main {
	public static void main(String[] args) throws Exception {
		SevenSingleton sevenSingleton1 = SevenSingleton.getInstance();
		SevenSingleton sevenSingleton2 = SevenSingleton.getInstance();
		SevenSingleton sevenSingleton3 = SevenSingleton.getInstance();
		SevenSingleton sevenSingleton4 = SevenSingleton.getInstance();
		SevenSingleton sevenSingleton5 = SevenSingleton.getInstance();
		SevenSingleton sevenSingleton6 = SevenSingleton.getInstance();
		SevenSingleton sevenSingleton7 = SevenSingleton.getInstance();
		SevenSingleton sevenSingleton8 = SevenSingleton.getInstance();
	}
}
