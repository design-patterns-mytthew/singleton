package eu.mytthew;

public class SevenSingleton {
	private static SevenSingleton[] instances = new SevenSingleton[7];

	private SevenSingleton() {
	}

	static SevenSingleton getInstance(int index) {
		if (instances[index] == null) {
			instances[index] = new SevenSingleton();
		}
		return instances[index];
	}
}
